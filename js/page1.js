class Product {
    constructor(productNumber, description, price, fee, quantity) {
      this.productNumber = productNumber;
      this.description = description;
      this.price = price;
      this.fee = fee;
      this.quantity = quantity;
    }
}

var product1 = new Product(01, "Product One", 250, 200, 8);
var product2 = new Product(02, "Product Two", 500, 50, 0);
var product3 = new Product(03, "Product Three", 320, 0, 6);
var product4 = new Product(04, "Product Four", 45, 65, 16);
var product5 = new Product(05, "Product Five", 1200, 300, 2);
var product6 = new Product(06, "Product Six", 75, 0, 0);

var products = [product1, product2, product3, product4, product5, product6];

function getShuffledUniqueImages(myArrayOfImages)
{
    var randImages;
    var arrayLength = myArrayOfImages.length;
    // make 6 random integers with no duplicates
    var randomInt1 = Math.floor(Math.random(arrayLength/6));
    var randomInt2 = Math.floor(Math.random(arrayLength/6)+((1/6)*arrayLength));
    var randomInt3 = Math.floor(Math.random(arrayLength/6)+((2/6)*arrayLength));
    var randomInt4 = Math.floor(Math.random(arrayLength/6)+((3/6)*arrayLength));
    var randomInt5 = Math.floor(Math.random(arrayLength/6)+((4/6)*arrayLength));
    var randomInt6 = Math.floor(Math.random(arrayLength/6)+((5/6)*arrayLength));

    var img1 = myArrayOfImages[randomInt1];
    var img2 = myArrayOfImages[randomInt2];
    var img3 = myArrayOfImages[randomInt3];
    var img4 = myArrayOfImages[randomInt4];
    var img5 = myArrayOfImages[randomInt5];
    var img6 = myArrayOfImages[randomInt6];
    randImages = [img1, img2, img3, img4, img5, img6];

    return randImages; // Will be used as "arrayOfPictureNames" in the function 'productionTableWithParameters'
}

function productionTableWithParameters(arrayOfProducts, arrayOfPictureNames)
{
    var strTable = "<table border='1'><thead><th>Product Number</th><th>Description</th><th>Available Qty</th><th>Price</th><th>Fee</th><th>Total</th><th>Image</th></thead>";
    for (var index = 0; index < products.length; index++)
    {
        strTable += "<tr>";

        strTable += "<td>" + arrayOfProducts[index].productNumber + "</td>";
        strTable += "<td>" + arrayOfProducts[index].description + "</td>";
        
        
        if (arrayOfProducts[index].quantity == 0)
        {
            strTable += "<td bgcolor='#FF6464'>" + arrayOfProducts[index].quantity + "</td>";
        } else {
            strTable += "<td>" + arrayOfProducts[index].quantity + "</td>";
        }
        
        strTable += "<td>" + arrayOfProducts[index].price + "</td>";

        if (arrayOfProducts[index].fee > 0)
        {
            strTable += '<td bgcolor="lightgrey">' + arrayOfProducts[index].fee + '</td>';
        } else {
            strTable += '<td>' + arrayOfProducts[index].fee + "</td>";
        }
        
        strTable += "<td>" + ((arrayOfProducts[index].price + arrayOfProducts[index].fee) * arrayOfProducts[index].quantity) + "</td>";
        strTable += "<td>" + "<img src='images/" + arrayOfPictureNames[index] + "' height='80' width=auto>" + "</td>";

        strTable += "</tr>";
    }
    strTable += "</table>";

    return strTable;
}

var imageSet = getShuffledUniqueImages(pictures);

document.getElementById('mytable').innerHTML = productionTableWithParameters(products, imageSet);
console.log(productionTableWithParameters(products, imageSet));